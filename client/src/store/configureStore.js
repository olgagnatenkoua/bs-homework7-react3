import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "../reducers";
import rootSaga from "../sagas/index";
import { composeWithDevTools } from "redux-devtools-extension";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";

import socketMiddleware from "../ChatPage/middleware/socket.middleware";

export const history = createBrowserHistory();

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer(history),
    undefined,
    composeWithDevTools(
      // applyMiddleware(routerMiddleware(history), sagaMiddleware, socketMiddleware.emitChatUpdateMiddleware)
      applyMiddleware(routerMiddleware(history), sagaMiddleware, socketMiddleware.emitChatUpdateMiddleware)
    )
  );

  // socketMiddleware.receiveChatUpdateMiddleware(store);
  sagaMiddleware.run(rootSaga);

  return store;
}
