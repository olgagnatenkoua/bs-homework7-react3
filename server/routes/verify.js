const express = require("express");
const router = express.Router();
const userService = require("../services/user.service");

router.post("/", function(req, res) {
  const result = userService.verify(req.body);
  if (!result) {
    res.status(500).send("Server error: cannot process user verification");
    return;
  }
  result.valid ? res.status(200).send(result) : res.status(400).send(result);
});

module.exports = router;
