import React, { Component } from "react";

export default class UserItem extends Component {
  render() {
    const { id, name, surname, email } = this.props;
    return (
      <div className="container list-group-item">
        <div className="row align-left">
          <div className="col-4">
            <span className="badge float-left text-lg">
              {name} {surname}
            </span>
          </div>
          <div className="col-4">
            <span className="badge text-lg">{email}</span>
          </div>
          <div className="col-4 btn-group">
            <button
              className="btn btn-outline-secondary"
              onClick={e => this.props.onEdit(id)}
            >
              {" "}
              Edit{" "}
            </button>
            <button
              className="btn btn-outline-secondary"
              onClick={e => this.props.onDelete(id)}
            >
              {" "}
              Delete{" "}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
