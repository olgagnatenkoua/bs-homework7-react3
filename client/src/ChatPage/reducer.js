import {
  likeMessageRoutine,
  editMessageRoutine,
  deleteMessageRoutine,
  addMessageRoutine,
  fetchMessagesRoutine,
  setCurrentUserRoutine
} from "./routines";

import initialState from "./helpers/initialState";

const enrichMessages = (user, messages) => {
  return messages.map(msg => {
    return {
      ...msg,
      ownedByMe: user._id === msg.userId,
      likedByMe: msg.likedBy.includes(user._id)
    };
  });
};

export default function(state = initialState, action) {
  switch (action.type) {
    case setCurrentUserRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }

    case setCurrentUserRoutine.SUCCESS: {
      return {
        ...state,
        loading: false,
        user: action.payload.user
      };
    }

    case setCurrentUserRoutine.FAILURE: {
      return {
        ...state,
        loading: false,
        user: null,
        error: action.payload
      };
    }

    case fetchMessagesRoutine.REQUEST: {
      return {
        ...state,
        loading: true,
        shouldScrollToBottom: true
      };
    }

    case fetchMessagesRoutine.SUCCESS: {
      const { messages, user } = action.payload;
      const enrichedMessages = enrichMessages(user, messages);
      const participants = new Set(messages.map(msg => msg.userId));
      let lastMessage = "Unknown";
      const lastMessageIdx = messages.length - 1;
      if (messages && messages[lastMessageIdx]) {
        lastMessage = new Date(
          messages[lastMessageIdx].createdAt + " GMT+0000"
        );
      }

      return {
        ...state,
        loading: false,
        messages: enrichedMessages,
        header: {
          participants: participants.size + 1,
          messages: messages.length,
          lastMessage
        },
        shouldScrollToBottom: false
      };
    }

    case fetchMessagesRoutine.FAILURE: {
      return {
        ...state,
        messages: [],
        error: action.payload,
        loading: false,
        shouldScrollToBottom: false
      };
    }

    case likeMessageRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }

    case likeMessageRoutine.SUCCESS: {
      const { message } = action.payload;
      const { messages, user } = state;

      const updatedMessages = [...messages];
      const likedMessageIndex = updatedMessages.findIndex(
        msg => msg._id === message._id
      );
      if (likedMessageIndex === -1) {
        return state;
      }
      message.likedByMe = message.likedBy.includes(user._id);
      updatedMessages[likedMessageIndex] = message;
      return {
        ...state,
        loading: false,
        messages: updatedMessages
      };
    }

    case likeMessageRoutine.FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case deleteMessageRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }

    case deleteMessageRoutine.SUCCESS: {
      const { message } = action.payload;
      const { messages } = state;
      const updatedMessages = [...messages];
      const deletedMessageIndex = updatedMessages.findIndex(
        msg => msg._id === message._id
      );
      if (deletedMessageIndex === -1) {
        return state;
      }
      updatedMessages.splice(deletedMessageIndex, 1);
      return {
        ...state,
        loading: false,
        messages: updatedMessages
      };
    }

    case deleteMessageRoutine.FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case addMessageRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }

    case addMessageRoutine.SUCCESS: {
      const { message } = action.payload;
      const { messages, user } = state;
      const enrichedMessage = enrichMessages(user, [message]);
      const updatedMessages = [...messages, ...enrichedMessage];
      return {
        ...state,
        loading: false,
        messages: updatedMessages
      };
    }

    case addMessageRoutine.FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case editMessageRoutine.TRIGGER: {
      return {
        ...state,
        loading: false,
        editedMessage: action.payload.message
      };
    }

    case editMessageRoutine.REQUEST: {
      return {
        ...state,
        editedMessage: action.payload.message,
        loading: true
      };
    }

    case editMessageRoutine.SUCCESS: {
      const { message } = action.payload;
      const { messages } = state;
      const updatedMessages = [...messages];
      const updatedMessageIndex = updatedMessages.findIndex(
        msg => msg._id === message._id
      );
      if (updatedMessageIndex === -1) {
        return state;
      }
      updatedMessages[updatedMessageIndex] = message;
      return {
        ...state,
        messages: updatedMessages,
        editedMessage: null,
        loading: false
      };
    }

    case editMessageRoutine.FAILURE: {
      return {
        ...state,
        loading: false,
        editedMessage: null,
        error: action.payload
      };
    }

    case editMessageRoutine.FULFILL: {
      return {
        ...state,
        loading: false,
        editedMessage: null
      };
    }

    default: {
      return state;
    }
  }
}
