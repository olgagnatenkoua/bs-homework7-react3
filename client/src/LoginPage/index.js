import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./actions";

import PasswordInput from "../shared/inputs/password/PasswordInput";
import EmailInput from "../shared/inputs/email/EmailInput";
import stages from "../shared/stages";
import Spinner from "../shared/display/Spinner/Spinner";

import "./bootstrap.min.css";
import "./LoginPage.css";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: ""
      }
    };
  }

  componentDidMount() {
    // visit to login page equals to logout 
    sessionStorage.removeItem("user");
  }

  componentDidUpdate() {
    const { auth, data } = this.props;
    if (auth) {
      this.persistUser(data);
      const redirectLocation = data.admin ? "/users" : "/chat";
      this.props.history.push(redirectLocation);
    }
  }

  // TODO: find out how to distinguish error 400x from error 500x in saga try/catch
  // renderAuthFailed = () => {
  //   if (!this.props.auth || this.props.auth.ok) {
  //     return null;
  //   }
  //   return (
  //     <div id="auth-failed" className="alert alert-danger" role="alert">
  //       Server error. Please refresh the page and try again
  //     </div>
  //   );
  // };

  persistUser = user => {
    sessionStorage.setItem("user", JSON.stringify(user));
  };

  renderAuthError = () => {
    if (!this.props.error) {
      return null;
    }
    return (
      <div id="auth-error" className="alert alert-danger" role="alert">
        Login failed. Please check your credentials and retry.
      </div>
    );
  };

  onChangeData = async (e, keyword) => {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        [keyword]: value
      }
    });
  };

  onLogin = e => {
    e.preventDefault();
    const { data } = this.state;
    this.props.loginUser(stages.REQUEST, data);
    this.setState({
      data: {
        email: "",
        password: ""
      }
    });
    return false;
  };

  render() {
    const { data } = this.state;
    if (this.props.loading) {
      return (<div className="spinner__external"><Spinner /></div>);
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card card-signin my-5">
              <div className="card-body">
                <h5 className="card-title text-center">User Login</h5>
                <form className="form-signin" id="login" name="login">
                  <EmailInput
                    label="Email"
                    type="text"
                    text={data["email"]}
                    keyword="email"
                    onChange={this.onChangeData}
                  />

                  <div className="form-label-group">
                    <PasswordInput
                      label="Password"
                      type="password"
                      text={data["password"]}
                      keyword="password"
                      onChange={this.onChangeData}
                    />
                  </div>

                  <button
                    className="btn btn-lg btn-outline-primary text-uppercase"
                    type="submit"
                    onClick={this.onLogin}
                  >
                    Login
                  </button>
                </form>
                {/* {this.renderAuthFailed()} */}
                {this.renderAuthError()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {};

const mapStateToProps = ({ loginPage }) => ({
  auth: loginPage.auth,
  error: loginPage.error,
  loading: loginPage.loading,
  data: loginPage.data
});

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
