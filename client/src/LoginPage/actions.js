import { loginUserRoutine } from "./routines";

export const loginUser = (stage, data) =>
  loginUserRoutine[stage]({
    data
  });
