import React from "react";
import "./Spinner.css";

const Spinner = () => (
  <div className="spinner__container">
    <div className="spinner"/>
  </div>
);

export default Spinner;
