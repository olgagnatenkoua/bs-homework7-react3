import { all } from "redux-saga/effects";
import userPageSagas from "../userPage/sagas";
import usersSagas from "../users/sagas";
import loginSagas from "../LoginPage/sagas";
import chatPageSagas from "../ChatPage/sagas";


export default function* rootSaga() {
  yield all([userPageSagas(), usersSagas(), loginSagas(), chatPageSagas()]);
}
