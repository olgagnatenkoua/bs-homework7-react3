export default {
  user: null,
  messages: [],
  header: {
    participants: 0,
    messages: 0,
    lastMessageAt: null
  },
  launchedAt: Date.now().toString(),
  editedMessage: null,
  loading: false,
  socket: null,
  shouldScrollToBottom: false
};
