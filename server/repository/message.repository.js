const fs = require("fs");
const validUrl = require("valid-url");
const constants = require("../helpers/constants");

// for simplicity purposes, message contains user name, email avatar as a separate field; todo: rework thi to get user data from users list

// todo: rewrite Delete to non-disruptive: set "active" false for deleted records

class MessageRepository {
  constructor() {
    this.messages = this.loadMessages();
  }

  loadMessages = () => {
    const jsonContent = fs.readFileSync("./data/messages.json");
    return JSON.parse(jsonContent);
  };

  getMessages = () => {
    if (this.messages) {
      return this.messages;
    } else {
      return null;
    }
  };

  getMessageById = _id => this.messages.find(msg => msg._id === _id);

  addMessage = newMessage => {
    const validationResult = this.validateMessageFields(newMessage);
    if (!validationResult.ok) {
      return validationResult;
    }
    newMessage = {
      ...newMessage,
      _id: Date.now().toString(),
      createdAt: new Date().toGMTString("en-us"),
      likedBy: []
    };
    this.messages.push({
      ...newMessage
    });
    return {
      ok: true,
      data: newMessage
    };
  };

  updateMessage = newMessage => {
    const validationResult = this.validateMessageFields(newMessage);
    if (!validationResult.ok) {
      return validationResult;
    }
    let ok = false;

    const { _id } = newMessage;
    const msgIndex = this.messages.findIndex(msg => msg._id === _id);
    if (msgIndex === -1) {
      return {
        ok,
        error: `Message with id=${_id} not found`
      };
    }
    this.messages[msgIndex] = { ...newMessage };
    return {
      ok: true,
      data: newMessage
    };
  };

  deleteMessage = _id => {
    const msgIndex = this.messages.findIndex(msg => msg._id === _id);
    if (msgIndex === -1) {
      return {
        ok: false,
        error: `Message with id=${_id} not found`
      };
    }
    const deletedMessage = this.messages.splice(msgIndex, 1)[0];
    return {
      ok: true,
      data: deletedMessage
    };
  };

  likeMessage = (user, _id) => {
    const msgIndex = this.messages.findIndex(msg => msg._id === _id);
    if (msgIndex === -1) {
      return {
        ok: false,
        error: `Message with id=${_id} not found`
      };
    }
    const userId = user._id;
    const { likedBy } = this.messages[msgIndex];
    if (!likedBy.includes(userId)) {
      likedBy.push(userId);
    } else {
      const userIdIndex = likedBy.findIndex((msg) => userId === msg.userId);
      likedBy.splice(userIdIndex, 1);
    }

    return {
      ok: true,
      data: Object.assign({}, this.messages[msgIndex])
    }
  };

  validateMessageFields = msg => {
    // it is assumed that user id, email and avatar are already valid
    // so here we just chek that these fields are not empty
    let ok = false;
    const { userId, name, email, avatar, message } = msg;
    const requiredFields = { userId, name, email, avatar, message };
    for (let key in requiredFields) {
      if (!requiredFields[key]) {
        return {
          ok,
          error: `${key} is a required field and cannot be empty.`
        };
      }
    }

    return {
      ok: true,
      data: msg
    };
  };
}

module.exports = new MessageRepository();
