import {
  addUserRoutine,
  updateUserRoutine,
  deleteUserRoutine,
  fetchUsersRoutine
} from "./routines";

export const addUser = (stage, data, history) =>
  addUserRoutine[stage]({
    data,
    history
  });

export const updateUser = (stage, data, history) =>
  updateUserRoutine[stage]({
    data,
    history 
  });

export const deleteUser = (stage, id) =>
  deleteUserRoutine[stage]({
    id
  });

export const fetchUsers = stage => fetchUsersRoutine[stage]({});
