import React, { Component } from "react";
import Chat from "./components/Chat/Chat";
import "./ChatPage.css";

class ChatPage extends Component {
  render = () => {
    return (
      <div className="chat__wrapper">
        <header className="header">
          <div className="logo">
            <img src="https://previews.dropbox.com/p/thumb/AAfLXxCHXV7fccEScffgyGq9cXaHnvcAp07owOUEa6-mthhSeQ4lZE6YVSbVUpSFSwWuDVQkSYOToqx0n7MaPCdcSk7LUWKMLRw53itz7XifavgysjUupNBzlV4XnjzQGERwL8Gq94ASokPtlDsDDw2lZ-Nm9V44QoPY2WlmBoeBwJesKotwqkCvcRoCYfPFNwUACoY1OG6tZ9628YUgluQi2YaDRV-ACYdGcTgsT_nNP0c0yOmKAW-K1c7jp3uyophIXEIjWWy-cxhbVmfMED6lrBwzgtwsFEdqqqCI-ShZLj8FNf5Jj-UxGbMhdBLLR2wgysgBy9FbZ37AOeG8G-Re/p.jpeg?fv_content=true&size_mode=5" alt="logo" className="logo__img" />
            <span className="logo__text">YET ANOTHER CHAT :)</span>
          </div>
        </header>
        <Chat history={this.props.history}></Chat>
        <footer className="footer">
          <div className="copyright">
            <span>COPYRIGHT</span>
          </div>
        </footer>
      </div>
    );
  };
}

export default ChatPage;
