const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require('cors')
const http = require('http')

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const messagesRouter = require("./routes/messages");
const loginRouter = require("./routes/login");
const verifyRouter = require("./routes/verify");


import socketIO from 'socket.io';
// import socketInjector from './socket/injector';
import socketHandlers from './socket/handlers';


const app = express();
app.use(cors());

const socketServer = http.Server(app);
const io = socketIO(socketServer);

// const io = socketIO.listen(server);
io.on('connection', socketHandlers);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/messages", messagesRouter);
app.use("/login", loginRouter);
app.use("/verify", verifyRouter);


// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get("env") === "development" ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   // res.render('error');
// });

module.exports = app;
