import { push } from "connected-react-router";
import axios from "axios";
import api from "../shared/config/api";
import { call, put, takeEvery, all, delay } from "redux-saga/effects";
import {
  addUserRoutine,
  deleteUserRoutine,
  updateUserRoutine,
  fetchUsersRoutine
} from "./routines";

export function* fetchUsers() {
  try {
    const users = yield call(axios.get, `${api.url}/users`);
    yield put(fetchUsersRoutine.success({ users: users.data }));
  } catch (error) {
    console.log("fetchUsers error:", error.message);
    yield put(fetchUsersRoutine.failure({ error: error.message }));
  }
}

function* watchFetchUsers() {
  yield takeEvery(fetchUsersRoutine.REQUEST, fetchUsers);
}

export function* addUser(action) {
  const newUser = { ...action.payload.data };

  try {
    yield call(axios.post, `${api.url}/users`, newUser);
    yield put(addUserRoutine.success({ user: newUser }));
    yield put(action.payload.history.push("/users"));
    yield put(fetchUsersRoutine.request());
  } catch (error) {
    console.log("createUser error:", error.message);
    yield put(addUserRoutine.failure({ error: error.message }));
  }
}

function* watchAddUser() {
  yield takeEvery(addUserRoutine.REQUEST, addUser);
}

export function* updateUser(action) {
  const updatedUser = { ...action.payload.data };
  const { _id } = updatedUser;

  try {
    const user = yield call(axios.put, `${api.url}/users/${_id}`, updatedUser);
    yield put(updateUserRoutine.success({ user }));
    action.payload.history.push("/users");
    yield put(fetchUsersRoutine.request());
  } catch (error) {
    console.log("updateUser error:", error.message);
    yield put(updateUserRoutine.failure({ error: error.message }));
  }
}

function* watchUpdateUser() {
  yield takeEvery(updateUserRoutine.REQUEST, updateUser);
}

export function* deleteUser(action) {
  try {
    const deletedUser = yield call(
      axios.delete,
      `${api.url}/users/${action.payload.id}`
    );
    yield put(deleteUserRoutine.success({ user: deletedUser }));
    yield put(fetchUsersRoutine.request());
  } catch (error) {
    console.log("deleteUser Error:", error.message);
    yield put(deleteUserRoutine.failure({ error: error.message }));
  }
}

function* watchDeleteUser() {
  yield takeEvery(deleteUserRoutine.REQUEST, deleteUser);
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUser(),
    watchUpdateUser(),
    watchDeleteUser()
  ]);
}
