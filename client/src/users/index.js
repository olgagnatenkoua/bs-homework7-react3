import React, { Component } from "react";
import { connect } from "react-redux";
import UserItem from "./UserItem";
import * as actions from "./actions";
import PropTypes from "prop-types";

import stages from "../shared/stages";

import "./UserList.css";
import Spinner from "../shared/display/Spinner/Spinner";
import verifyUser from "../shared/helpers/verify";

class UserList extends Component {
  componentDidMount() {
    const user = verifyUser();
    if (user && user.admin) {
      this.props.fetchUsers(stages.REQUEST);
    } else {
      this.props.history.push("/");
    }
  }

  onEdit = id => {
    this.props.history.push(`/users/${id}`);
  };

  onDelete = id => {
    this.props.deleteUser(stages.REQUEST, id);
  };

  onAdd = () => {
    this.props.history.push("/users/add");
  };

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }
    return (
      <div className="row">
        <div className="list-group col-10 user__item">
          <div className="container list-group-item user__header">
            <div className="col-12 btn__container">
              <button
                className="btn btn-outline-primary btn__add"
                onClick={this.onAdd}
              >
                Add user
              </button>
            </div>
            <div className="row align-left">
              <div className="col-4">
                <span className="badge float-left text-lg">
                  User Name and Surname
                </span>
              </div>

              <div className="col-4">
                <span className="badge text-lg">Email</span>
              </div>
              <div className="col-4">
                <span className="badge float-left text-lg">Manage User</span>
              </div>
            </div>
          </div>
          {this.props.users.map(user => {
            return (
              <UserItem
                key={user._id}
                id={user._id}
                name={user.name}
                surname={user.surname}
                email={user.email}
                onEdit={this.onEdit}
                onDelete={this.onDelete}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.array,
  loading: PropTypes.bool
};

const mapStateToProps = ({ users }) => {
  return {
    loading: users.loading,
    users: users.users
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
