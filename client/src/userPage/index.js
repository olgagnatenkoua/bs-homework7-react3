import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./actions";
import { addUser, updateUser } from "../users/actions";
import TextInput from "../shared/inputs/text/TextInput";
import PasswordInput from "../shared/inputs/password/PasswordInput";
import EmailInput from "../shared/inputs/email/EmailInput";
import userFormConfig from "../shared/config/userFormConfig";
import defaultUserConfig from "../shared/config/defaultUserConfig";
import PropTypes from "prop-types";
import stages from "../shared/stages";
import Spinner from "../shared/display/Spinner/Spinner";
import verifyUser from "../shared/helpers/verify";

const EMAIL = "email";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultUserData();
  }

  componentDidMount() {
    const user = verifyUser();
    if (!(user && user.admin)) {
      this.props.history.push("/");
      return;
    }

    if (this.props.match.params.id) {
      this.props.fetchUser(stages.REQUEST, this.props.match.params.id);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!nextProps.userData) {
      return null;
    }
    if (nextProps.userData._id !== prevState._id && nextProps.match.params.id) {
      return {
        ...nextProps.userData
      };
    } else {
      return null;
    }
  }

  onCancel = () => {
    this.setState(this.getDefaultUserData());
    this.props.history.push("/users");
  };

  onSave = () => {
    const { userData } = this.props;
    if (userData && userData._id) {
      this.props.updateUser(stages.REQUEST, this.state, this.props.history);
    } else {
      this.props.addUser(stages.REQUEST, this.state, this.props.history);
    }
  };

  onChangeData = async (e, keyword) => {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      [keyword]: value
    });
  };

  getDefaultUserData = () => {
    return {
      ...defaultUserConfig
    };
  };

  getInput = (data, { label, type, keyword }, index) => {
    switch (type) {
      case "text":
        return (
          <TextInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={this.onChangeData}
          />
        );
      case "email":
        return (
          <EmailInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            ref={EMAIL}
            onChange={this.onChangeData}
          />
        );
      case "password":
        return (
          <PasswordInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={this.onChangeData}
          />
        );
      default:
        return null;
    }
  };

  renderAddEditError = () => {
    if (!this.props.addEditError) {
      return null;
    }
    const editMode = !!this.props.userData;
    return (
      <div id="auth-error" className="alert alert-danger" role="alert">
        <p>
          {editMode ? "Edit" : "Add"} action failed: please check your data and
          retry.
        </p>
      </div>
    );
  };

  render() {
    const data = this.state;
    const { userData } = this.props;
    const editedUserId = userData && userData._id;
    if (this.props.loading) {
      return <Spinner />;
    }
    return (
      <div
        className="modal"
        style={{ display: "block" }}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{ padding: "5px" }}>
            <div className="modal-header">
              <h5 className="modal-title">
                {editedUserId ? "Edit user" : "Add user"}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.onCancel}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {userFormConfig.map((item, index) =>
                this.getInput(data, item, index)
              )}
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={this.onCancel}>
                Cancel
              </button>
              <button className="btn btn-primary" onClick={this.onSave}>
                Save
              </button>
            </div>
          </div>
        </div>
        {this.renderAddEditError()}
      </div>
    );
  }
}

UserPage.propTypes = {
  userData: PropTypes.object
};

const mapStateToProps = state => {
  const { userPage, users } = state;
  return {
    userData: userPage.userData,
    loading: userPage.loading || users.loading,
    addEditError: users.addEditError
  };
};

const mapDispatchToProps = {
  ...actions,
  addUser,
  updateUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPage);
