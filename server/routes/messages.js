const express = require("express");
const router = express.Router();
const messageRepository = require("../repository/message.repository");

router.get("/", function(req, res) {
  const result = messageRepository.getMessages();
  result
    ? res.send(result)
    : res.status(500).send("Server error: cannot get messages");
});

router.get("/:id", function(req, res) {
  const { id } = req.params;
  const result = messageRepository.getMessageById(id);
  result
    ? res.status(200).send(result)
    : res.status(404).send(`Cannot find message with id ${id}`);
});

router.post("/", function(req, res) {
  const result = messageRepository.addMessage(req.body);
  if (!result) {
    res.status(500).send("Server error: cannot add message");
    return;
  }
  result.ok
    ? res.status(201).send(result.data)
    : res.status(400).send("Cannot add message");
});

router.delete("/:id", function(req, res) {
  const { id } = req.params;
  const result = messageRepository.deleteMessage(id);
  if (!result) {
    res.status(500).send("Server error: cannot delete message");
    return;
  }
  result.ok
    ? res.status(200).send(result.data)
    : res.status(404).send(result.error);
});

router.put("/:id", function(req, res) {
  const { id } = req.params;
  const result = messageRepository.updateMessage(req.body);
  if (!result) {
    res.status(500).send(`Server error: cannot update message`);
    return;
  }
  result.ok
    ? res.status(200).send(result.data)
    : res.status(404).send(`Cannot update message with id ${id}`);
});

router.put("/:id/like", function(req, res) {
  const user = req.body;
  const { id } = req.params;
  const result = messageRepository.likeMessage(user, id);
  if (!result) {
    res.status(500).send(`Server error: cannot process message like`);
    return;
  }
  result.ok
    ? res.status(200).send(result.data)
    : res.status(404).send(`Cannot change like for message with id ${id}`);
});

module.exports = router;
