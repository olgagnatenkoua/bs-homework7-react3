import { loginUserRoutine } from "./routines";

const initialState = {
  auth: false,
  data: {},
  error: null,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case loginUserRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }
    case loginUserRoutine.FAILURE: {
      return {
        ...state,
        auth: false,
        data: {},
        error: action.payload,
        loading: false
      };
    }
    case loginUserRoutine.SUCCESS: {
      const { auth, data } = action.payload;
      return {
        ...state,
        auth,
        data,
        loading: false
      };
    }

    default:
      return state;
  }
}
