export default {
  TRIGGER: "trigger",
  REQUEST: "request",
  FAILURE: "failure",
  SUCCESS: "success",
  FULFILL: "fulfill"
};
