const express = require("express");
const router = express.Router();
const userService = require("../services/user.service");

router.post("/", function(req, res) {
  const { email, password } = req.body;
  const result = userService.handleLogin(email, password);
  if (!result) {
    res.status(500).send("Server error: cannot process user login");
    return;
  }
  result.auth ? res.status(200).send(result) : res.status(400).send(result);
});

module.exports = router;
