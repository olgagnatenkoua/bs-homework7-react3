import axios from "axios";
import api from "../shared/config/api";
import { call, put, takeEvery, all, delay } from "redux-saga/effects";
import { fetchUserRoutine } from "./routines";

export function* fetchUser(action) {
  try {
    yield delay(1000);
    const user = yield call(axios.get, `${api.url}/users/${action.payload.id}`);
    yield put(fetchUserRoutine.success({userData: user.data}));
  } catch (error) {
    console.log("fetchUser error:", error.message);
    yield put(fetchUserRoutine.failure({ error: error.message }));
  }
}

function* watchFetchUser() {
  yield takeEvery(fetchUserRoutine.REQUEST, fetchUser);
}

export default function* userPageSagas() {
  yield all([watchFetchUser()]);
}
