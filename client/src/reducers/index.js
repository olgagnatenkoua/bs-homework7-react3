import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import users from "../users/reducer";
import userPage from "../userPage/reducer";
import loginPage from "../LoginPage/reducer";
import chatPage from "../ChatPage/reducer";

export default history =>
  combineReducers({
    router: connectRouter(history),
    users,
    userPage,
    loginPage,
    chatPage
  });
