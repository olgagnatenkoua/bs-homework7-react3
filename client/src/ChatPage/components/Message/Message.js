import React from "react";
import "./Message.css";

class Message extends React.Component {
  renderAvatar = ({ ownedByMe, avatar }) => {
    if (!ownedByMe) {
      return (
        <div className="message__avatar">
          <img src={avatar} alt="avatar" />
        </div>
      );
    }
  };

  renderAside = ({ createdAt, likedBy, ownedByMe, likedByMe, _id }) => {
    const displayTime = new Date(createdAt + " GMT+0000").toLocaleTimeString(
      "en-US"
    );
    const likes = likedBy ? likedBy.length : 0; 
    return (
      <div className="message__aside">
        <div className="message__time">{displayTime}</div>
        <div className="message__likes" data-message-id={_id}>
          {this.renderLikes(likes)}
          {this.renderLikeBtn(ownedByMe, likedByMe)}
          {this.renderEditBtn(ownedByMe)}
          {this.renderDeleteBtn(ownedByMe)}
        </div>
      </div>
    );
  };

  renderEditBtn = ownedByMe => {
    return ownedByMe ? (
      <div className="message__edit" onClick={this.handleEditClick}>
        <i className="fas fa-edit" /> 
        {/* <span>EDIT</span>  */}
      </div>
    ) : null;
  };

  renderDeleteBtn = ownedByMe => {
    return ownedByMe ? (
      <div className="message__delete" onClick={this.handleDeleteClick}>
        <i className="fas fa-trash" /> 
        {/* <span>DELETE</span> */}
      </div>
    ) : null;
  };

  renderLikeBtn = (ownedByMe, likedByMe) => {
    const likeClass = `${likedByMe ? "liked" : ""} far fa-heart`;

    return !ownedByMe ? (
      <div className="message__like" onClick={this.handleLikeClick}>
        <i className={likeClass} />
        {/* <span>LIKE</span> */}
      </div>
    ) : null;
  };

  renderLikes = likes => {
    return likes ? (
      <div className="message__like-count">
        <i className="far fa-grin-hearts" />
        <span>{likes}</span>
      </div>
    ) : null;
  };

  findMsgId = evt => {
    try {
      const msgLikesElement = evt.target.parentNode.parentNode;
      return msgLikesElement.getAttribute("data-message-id");
    } catch (error) {
      console.log(error);
    }
  };

  handleLikeClick = evt => {
    evt.persist();
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.likeMessage(id);
  };

  handleEditClick = evt => {
    evt.persist();
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.editMessage(id);
  };

  handleDeleteClick = evt => {
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.deleteMessage(id);
  };

  render = () => {
    const { message } = this.props;
    const { ownedByMe } = message;

    const ownMessageClassName = ownedByMe ? " own-message" : "";
    const messageClassName = `message ${ownMessageClassName}`;

    return (
      <article className={messageClassName}>
        <div className="message__body">
          {this.renderAvatar(message)}
          <div className="message__text">{message.message}</div>
        </div>
        {this.renderAside(message, this.handleLikeClick)}
      </article>
    );
  };
}

export default Message;
