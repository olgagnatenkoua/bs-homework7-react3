import { createRoutine } from "redux-saga-routines";

export const LOGIN_USER = "LOGIN_USER";
export const loginUserRoutine = createRoutine(LOGIN_USER);
