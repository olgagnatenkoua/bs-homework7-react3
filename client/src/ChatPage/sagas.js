import axios from "axios";
import api from "../shared/config/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  setCurrentUserRoutine,
  fetchMessagesRoutine,
  likeMessageRoutine,
  deleteMessageRoutine,
  addMessageRoutine,
  editMessageRoutine
} from "./routines";
import { select } from "redux-saga/effects";

export function* setCurrentUser(action) {
  try {
    const { user, history } = action.payload;
    const response = yield call(axios.post, `${api.url}/verify`, user);
    const { valid } = response.data;
    if (valid) {
      yield put(setCurrentUserRoutine.success({ user }));
      yield put(fetchMessagesRoutine.request());
    } else {
      yield put(setCurrentUserRoutine.failure({ error: "User is not valid" }));
      history.push("/login");
    }
  } catch (error) {
    yield put(setCurrentUserRoutine.failure({ error: error.message }));
    action.payload.history.push("/login");
  }
}

export function* likeMessage(action) {
  try {
    const { user, id } = action.payload;
    const response = yield call(
      axios.put,
      `${api.url}/messages/${id}/like`,
      user
    );
    const { data } = response;
    if (data) {
      yield put(likeMessageRoutine.success({ message: data }));
      // emit to socket
    } else {
      yield put(likeMessageRoutine.failure({ error: response.error }));
    }
  } catch (error) {
    yield put(likeMessageRoutine.failure({ error: error.message }));
  }
}

export function* deleteMessage(action) {
  try {
    const { user, id } = action.payload;
    const { data: message } = yield call(
      axios.get,
      `${api.url}/messages/${id}`
    );
    if (message.userId !== user._id) {
      yield put(
        deleteMessageRoutine.failure({
          error: "User cannot delete messages of others"
        })
      );
      return;
    }
    const response = yield call(axios.delete, `${api.url}/messages/${id}`);
    const { data } = response;
    if (data) {
      yield put(deleteMessageRoutine.success({ message: data }));
      // emit to socket
    } else {
      yield put(deleteMessageRoutine.failure({ error: response.error }));
    }
  } catch (error) {
    yield put(deleteMessageRoutine.failure({ error: error.message }));
  }
}

export function* addMessage(action) {
  try {
    const { message } = action.payload;
    const response = yield call(axios.post, `${api.url}/messages`, message);
    const { data } = response;
    if (data) {
      yield put(addMessageRoutine.success({ message: data }));
      // emit to socket
    } else {
      yield put(addMessageRoutine.failure({ error: response.error }));
    }
  } catch (error) {
    yield put(addMessageRoutine.failure({ error: error.message }));
  }
}

export function* editMessage(action) {
  try {
    const { user, message } = action.payload;
    if (message.userId !== user._id) {
      yield put(
        editMessageRoutine.failure({
          error: "User cannot edit messages of others"
        })
      );
      return;
    }
    const { _id } = message;
    const response = yield call(
      axios.put,
      `${api.url}/messages/${_id}`,
      message
    );
    const { data } = response;
    if (data) {
      yield put(editMessageRoutine.success({ message: data }));
      // emit to socket
    } else {
      yield put(editMessageRoutine.failure({ error: response.error }));
    }
  } catch (error) {
    yield put(editMessageRoutine.failure({ error: error.message }));
  }
}

export function* fetchMessages() {
  try {
    const response = yield call(axios.get, `${api.url}/messages`);
    const messages = response.data;
    const user = yield select(state => state.chatPage.user);
    if (messages) {
      yield put(fetchMessagesRoutine.success({ messages, user }));
    } else {
      yield put(
        fetchMessagesRoutine.failure({
          error: `Error getting messages from server`
        })
      );
    }
  } catch (error) {
    yield put(fetchMessagesRoutine.failure({ error: error.message }));
  }
}

function* watchSetCurrentUser() {
  yield takeEvery(setCurrentUserRoutine.REQUEST, setCurrentUser);
}

function* watchLikeMessageRoutine() {
  yield takeEvery(likeMessageRoutine.REQUEST, likeMessage);
}

function* watchDeleteMessageRoutine() {
  yield takeEvery(deleteMessageRoutine.REQUEST, deleteMessage);
}

function* watchAddMessageRoutine() {
  yield takeEvery(addMessageRoutine.REQUEST, addMessage);
}

function* watchFetchMessagesRoutine() {
  yield takeEvery(fetchMessagesRoutine.REQUEST, fetchMessages);
}

function* watchEditMessageRoutine() {
  yield takeEvery(editMessageRoutine.REQUEST, editMessage);
}

export default function* chatSagas() {
  yield all([
    watchSetCurrentUser(),
    watchLikeMessageRoutine(),
    watchDeleteMessageRoutine(),
    watchAddMessageRoutine(),
    watchEditMessageRoutine(),
    watchFetchMessagesRoutine()
  ]);
}
