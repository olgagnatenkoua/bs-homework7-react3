const fs = require("fs");
const userRepository = require("../repository/user.repository");

class UserService {
  constructor() {
    this.admins = this.loadAdmins();
  }

  loadAdmins = () => {
    const jsonContent = fs.readFileSync("./data/admins.json");
    return JSON.parse(jsonContent);
  };

  verify = user => {
    if (!user) {
      return { valid: false };
    }
    const userInDb = userRepository.getUserById(user._id);
    if (!userInDb) {
      return { valid: false };
    }
    let valid = true;
    const fieldsToVerify = ["_id", "name", "surname", "email", "avatar"];
    fieldsToVerify.forEach(field => {
      valid = valid && user[field] === userInDb[field];
    });
    return {
      valid
    };
  };

  handleLogin = (email, password) => {
    if (!email) {
      return {
        auth: false,
        error: "Email cannot be empty"
      };
    }

    if (!password) {
      return {
        auth: false,
        error: "Password cannot be empty"
      };
    }

    const user = userRepository.getUserByEmail(email);
    if (!user || user.password !== password) {
      // use one message to avoid disclosing if email or password is wrong
      return {
        auth: false,
        error: `User with such credentials does not exist`
      };
    }

    const admin = this.admins.findIndex(email => user.email === email) !== -1;
    const responseUser = {...user}; 
    delete responseUser.password;

    return {
      auth: true,
      data: {
        ...responseUser,
        admin
      }
    };
  };
}

module.exports = new UserService();
