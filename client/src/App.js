import React from "react";
import UserList from "./users/index";
import UserPage from "./userPage/index";
import LoginPage from "./LoginPage/index";
import ChatPage from "./ChatPage/index";


import { Switch, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={LoginPage} />
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/users" component={UserList} />
        <Route exact path="/users/add" component={UserPage} />
        <Route path="/users/:id" component={UserPage} />
        <Route path="/chat" component={ChatPage} />
      </Switch>
    </div>
  );
}

export default App;
