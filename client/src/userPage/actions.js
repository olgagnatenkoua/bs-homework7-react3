import { fetchUserRoutine } from "./routines";

export const fetchUser = (stage, id) => fetchUserRoutine[stage]({ id });
