import { createRoutine } from "redux-saga-routines";

const LIKE_MESSAGE = "LIKE_MESSAGE";
const EDIT_MESSAGE = "EDIT_MESSAGE";
const DELETE_MESSAGE = "DELETE_MESSAGE";
const ADD_MESSAGE = "ADD_MESSAGE";
const FETCH_MESSAGES = "FETCH_MESSAGES";
const SET_CURRENT_USER = "SET_CURRENT_USER";

export const likeMessageRoutine = createRoutine(LIKE_MESSAGE);
export const editMessageRoutine = createRoutine(EDIT_MESSAGE);
export const deleteMessageRoutine = createRoutine(DELETE_MESSAGE);
export const addMessageRoutine = createRoutine(ADD_MESSAGE);
export const fetchMessagesRoutine = createRoutine(FETCH_MESSAGES);
export const setCurrentUserRoutine = createRoutine(SET_CURRENT_USER);

