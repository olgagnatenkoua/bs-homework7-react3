import io from "socket.io-client";
import api from "../../shared/config/api.json";
import {
  likeMessageRoutine,
  editMessageRoutine,
  deleteMessageRoutine,
  addMessageRoutine,
  fetchMessagesRoutine,
  setCurrentUserRoutine
} from "../routines";

class SocketMiddleWare {
  socket = null;

  emitChatUpdateMiddleware = store => {
    return next => action => {
      const result = next(action);
      //   if (this.socket && action.type === likeMessageRoutine.SUCCESS) {
      //     this.socket.emit("chat-action");
      //   }
      // todo: fix cors issue with websockets
      return result;
    };
  };

  receiveChatUpdateMiddleware = store => {
    this.socket = io.connect(api.SOCKET_SERVER);

    this.socket.on("chat-update", () => {
      store.dispatch(fetchMessagesRoutine.REQUEST); // todo: check if this works
    });
  };
}

const socketMiddleware = new SocketMiddleWare();
export default socketMiddleware;
