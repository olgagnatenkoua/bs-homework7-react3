import React, { useState } from "react";
import "./PasswordInput.css";
import PropTypes from "prop-types";

const PasswordInput = ({ text, keyword, label, onChange }) => {
  const [isShown, setIsShown] = useState(false);
  const inputType = isShown ? "text" : "password";

  const showPasswordClickHandler = e => {
    e.preventDefault();
    setIsShown(!isShown);
    return false;
  };

  return (
    <div className="form-group row password-input">
      <label className="col-sm-3 col-form-label">{label}</label>
      <input
        className="col-sm-7"
        value={text}
        type={inputType}
        onChange={e => onChange(e, keyword)}
      />
      <button className="col-sm-2" onClick={showPasswordClickHandler}>
        &#x1f441;
      </button>
    </div>
  );
};

PasswordInput.propTypes = {
  text: PropTypes.string,
  keyword: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func
};

export default PasswordInput;
