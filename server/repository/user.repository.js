const fs = require("fs");
const validUrl = require("valid-url");
const constants = require("../helpers/constants");

// todo: rewrite Delete to non-disruptive: set "active" false for deleted records

class UserRepository {
  constructor() {
    this.users = this.loadUsers();
  }

  loadUsers = () => {
    const jsonContent = fs.readFileSync("./data/users.json");
    return JSON.parse(jsonContent);
  };

  getUsers = () => {
    return this.users ? this.users : null;
  };

  getUserById = _id => this.users.find(user => user._id === _id);

  getUserByEmail = email => this.users.find(user => user.email === email);

  addUser = newUser => {
    if (newUser && !newUser.avatar) {
      newUser.avatar = constants.EMPTY_AVATAR_URI;
    }
    const validationResult = this.validateUserFields(newUser);
    if (!validationResult.ok) {
      return validationResult;
    }
    const emailExists = this.getUserByEmail(newUser.email);
    if (emailExists) {
      return {
        ok: false,
        error: "Cannot add user: user with such email already exists."
      };
    }
    newUser._id = Date.now().toString();
    this.users.push({
      ...newUser
    });
    return {
      ok: true,
      data: newUser
    };
  };

  updateUser = newUser => {
    const validationResult = this.validateUserFields(newUser);
    if (!validationResult.ok) {
      return validationResult;
    }
    let ok = false;

    const { _id } = newUser;
    const userIndex = this.users.findIndex(user => user._id === _id);
    if (userIndex === -1) {
      return {
        ok,
        error: `User with id=${_id} not found`
      };
    }
    this.users[userIndex] = { ...newUser };
    return {
      ok: true,
      data: newUser
    };
  };

  deleteUser = _id => {
    const userIndex = this.users.findIndex(user => user._id === _id);
    if (userIndex === -1) {
      return {
        ok: false,
        error: `User with id=${_id} not found`
      };
    }
    const deletedUser = this.users.splice(userIndex, 1)[0];
    return {
      ok: true,
      data: deletedUser
    };
  };

  isValidEmail = email => /^.+@.+\..+$/.test(email);

  validateUserFields = user => {
    let ok = false;
    const { name, surname, email, password, avatar } = user;
    const requiredTextFields = { name, surname, email, password };
    for (let key in requiredTextFields) {
      if (!requiredTextFields[key]) {
        return {
          ok,
          error: `${key} is a required field and cannot be empty`
        };
      }
    }

    if (!this.isValidEmail(email)) {
      return {
        ok,
        error: "Invalid email"
      };
    }

    if (!validUrl.isUri(avatar)) {
      return {
        ok,
        error: "Avatar should be a valid URI"
      };
    }

    return {
      ok: true,
      data: user
    };
  };
}

module.exports = new UserRepository();
