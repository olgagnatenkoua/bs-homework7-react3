import { createRoutine } from 'redux-saga-routines';

const ADD_USER = "ADD_USER";
const UPDATE_USER = "UPDATE_USER";
const DELETE_USER = "DELETE_USER";
const FETCH_USERS = "FETCH_USERS";

export const addUserRoutine = createRoutine(ADD_USER);
export const updateUserRoutine = createRoutine(UPDATE_USER);
export const deleteUserRoutine = createRoutine(DELETE_USER);
export const fetchUsersRoutine = createRoutine(FETCH_USERS);

