const express = require("express");
const router = express.Router();
const userRepository = require("../repository/user.repository");

router.get("/", function(req, res) {
  const result = userRepository.getUsers();
  result
    ? res.send(result)
    : res.status(500).send("Server error: cannot get users");
});

router.get("/:id", function(req, res) {
  const { id } = req.params;
  const result = userRepository.getUserById(id);
  result
    ? res.status(200).send(result)
    : res.status(404).send(`Cannot find user with id ${id}`);
});

router.post("/", function(req, res) {
  // fix the problem with axios.put being recognized as POST on some reason
  const result = req.body._id
    ? userRepository.updateUser(req.body)
    : userRepository.addUser(req.body);
  if (!result) {
    res.status(500).send("Server error: cannot add user");
    return;
  }
  result.ok
    ? res.status(201).send(result.data)
    : res.status(400).send(result.error);
});

router.delete("/:id", function(req, res) {
  const { id } = req.params;
  const result = userRepository.deleteUser(id);
  if (!result) {
    res.status(500).send("Server error: cannot delete user");
    return;
  }
  result.ok
    ? res.status(200).send(result.data)
    : res.status(404).send(result.error);
});

router.put("/:id", function(req, res) {
  const result = userRepository.updateUser(req.body);
  if (!result) {
    res.status(500).send(`Server error: cannot update user`);
    return;
  }
  result.ok
    ? res.status(200).send(result.data)
    : res.status(404).send(result.error);
});

module.exports = router;
