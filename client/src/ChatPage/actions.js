import {
  likeMessageRoutine,
  editMessageRoutine,
  deleteMessageRoutine,
  addMessageRoutine,
  fetchMessagesRoutine,
  setCurrentUserRoutine
} from "./routines";

export const setCurrentUser = (stage, user, history) =>
  setCurrentUserRoutine[stage]({
    user,
    history
  });

export const likeMessage = (stage, user, id) =>
  likeMessageRoutine[stage]({
    user,
    id
  });

export const editMessage = (stage, user, message) =>
  editMessageRoutine[stage]({
    user,
    message
  });

export const deleteMessage = (stage, user, id) =>
  deleteMessageRoutine[stage]({
    user,
    id
  });

export const addMessage = (stage, message) =>
  addMessageRoutine[stage]({
    message
  });

export const fetchMessages = (stage, user) =>
  fetchMessagesRoutine[stage]({
    user
  });

