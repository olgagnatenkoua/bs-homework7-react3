export default () => {
  const userJSON = sessionStorage.getItem("user");
  return userJSON ? JSON.parse(userJSON) : null;
};
