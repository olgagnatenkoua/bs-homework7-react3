import { fetchUserRoutine } from "./routines";
import { addUserRoutine, updateUserRoutine } from "../users/routines";

const initialUserData = {
  _id: "",
  name: "",
  surname: "",
  email: "",
  password: "",
  avatar: ""
};

export default function(state = { initialUserData }, action) {
  switch (action.type) {
    case fetchUserRoutine.REQUEST: {
      return {
        ...state,
        userData: initialUserData,
        loading: true
      };
    }

    case fetchUserRoutine.FAILURE: {
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    }
    case fetchUserRoutine.SUCCESS: {
      const { userData } = action.payload;
      return {
        ...state,
        userData,
        loading: false
      };
    }

    case addUserRoutine.SUCCESS: {
      return {
        ...state,
        userData: null,
        loading: false
      };
    }

    case updateUserRoutine.SUCCESS: {
      return {
        ...state,
        userData: null,
        loading: false
      };
    }

    // todo: on location change, empty userData 

    default:
      return state;
  }
}
