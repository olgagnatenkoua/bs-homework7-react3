import axios from "axios";
import api from "../shared/config/api";
import { call, put, takeEvery, all, delay } from "redux-saga/effects";
import { loginUserRoutine } from "./routines";

export function* loginUser(action) {
  try {
    const loginData = action.payload.data;
    yield delay(1000);
    const response = yield call(axios.post, `${api.url}/login`, loginData);
    const { auth, data } = response.data; 
    yield put(loginUserRoutine.success({ auth, data }));
  } catch (error) {
    yield put(loginUserRoutine.failure({ error: error.message }));
  }
}

function* watchLoginUser() {
  yield takeEvery(loginUserRoutine.REQUEST, loginUser);
}

export default function* loginPageSagas() {
  yield all([watchLoginUser()]);
}
