import {
  fetchUsersRoutine,
  deleteUserRoutine,
  addUserRoutine,
  updateUserRoutine
} from "./routines";

const initialState = {
  loading: true,
  users: [],
  addEditError: null,
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case fetchUsersRoutine.REQUEST: {
      return {
        ...state,
        loading: true,
        addEditError: null
      };
    }
    case fetchUsersRoutine.FAILURE: {
      return {
        ...state,
        error: action.payload,
        loading: false,
        addEditError: null
      };
    }
    case fetchUsersRoutine.SUCCESS: {
      return {
        ...state,
        users: [...action.payload.users],
        loading: false,
        addEditError: null
      };
    }

    case deleteUserRoutine.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }
    case deleteUserRoutine.FAILURE: {
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    }
    case deleteUserRoutine.SUCCESS: {
      return {
        ...state,
        loading: true
      };
    }

    case addUserRoutine.REQUEST: {
      return {
        ...state,
        addEditError: null,
        loading: true
      };
    }
    case addUserRoutine.FAILURE: {
      return {
        ...state,
        addEditError: action.payload,
        loading: false
      };
    }
    case addUserRoutine.SUCCESS: {
      return {
        ...state,
        loading: true,
        addEditError: null
      };
    }

    case updateUserRoutine.REQUEST: {
      return {
        ...state,
        addEditError: null,
        loading: true
      };
    }
    case updateUserRoutine.FAILURE: {
      return {
        ...state,
        addEditError: action.payload,
        loading: false
      };
    }
    case updateUserRoutine.SUCCESS: {
      return {
        ...state,
        loading: false,
        addEditError: null
      };
    }

    default:
      return state;
  }
}
