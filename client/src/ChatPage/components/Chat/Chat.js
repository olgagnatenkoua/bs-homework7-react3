import React from "react";
import { connect } from "react-redux";
import * as actions from "../../actions";

import Spinner from "../Spinner/Spinner";
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import MakeMessage from "../MakeMessage/MakeMessage";

import Modal from "../Modal/Modal";

import io from "socket.io-client";
import stages from "../../../shared/stages";

import api from "../../../shared/config/api.json";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.listRef = React.createRef();
  }

  // getSnapshotBeforeUpdate(prevProps, prevState) {
  //   // this logic should set scroll position at same level when user likes or deletes messages
  //   if (prevProps.messages.length === this.props.messages.length) {
  //     const list = this.listRef.current;
  //     debugger;
  //     if (!list) {
  //       return null;
  //     }
  //     return list.scrollHeight - list.scrollTop;
  //   }
  //   return null;
  // }

  componentDidMount() {
    const user = JSON.parse(sessionStorage.getItem("user"));
    const { history } = this.props;
    this.props.setCurrentUser(stages.REQUEST, user, history);
  }

  componentDidUpdate() {
    if (this.props.shouldScrollToBottom) {
      this.scrollToBottom();
    }
  }

  likeMessage = id => {
    this.props.likeMessage(stages.REQUEST, this.props.user, id);
  };

  addMessage = message => {
    this.props.addMessage(stages.REQUEST, message);
  };

  deleteMessage = id => {
    this.props.deleteMessage(stages.REQUEST, this.props.user, id);
  };

  editMessage = id => {
    const editedMessage = this.props.messages.find(msg => msg._id === id);
    if (!editedMessage) return;
    this.props.editMessage(stages.TRIGGER, this.props.user, editedMessage);
  };

  completeEditMessage = message => {
    this.props.editMessage(stages.REQUEST, this.props.user, message);
  };

  cancelEditMessage = () => {
    this.props.editMessage(stages.FULFILL);
  };

  scrollToBottom = () => {
    const chatElement = document.querySelector(".chat__body");
    if (chatElement) {
      chatElement.scrollTo(0, chatElement.scrollHeight);
    }
  };

  renderChat = ({ user, error, messages, header, launchedAt }) => {
    const handlers = {
      likeMessage: this.likeMessage,
      makeMessage: this.addMessage,
      deleteMessage: this.deleteMessage,
      editMessage: this.editMessage
    };
    const open = !!this.props.editedMessage;
    return (
      <div className="chat">
        <Header header={header} />
        <MessageList
          ref={this.listRef}
          messages={messages}
          launchedAt={launchedAt}
          handlers={handlers}
        />
        <MakeMessage makeMessage={this.addMessage} user={user} />
        <Modal
          open={open}
          title="Edit Message"
          onClose={this.cancelEditMessage}
        >
          <MakeMessage
            makeMessage={this.completeEditMessage}
            editedMessage={this.props.editedMessage}
            user={user}
            btnName="OK"
          />
        </Modal>
      </div>
    );
  };

  render() {
    const { loading } = this.props;
    return (
      <div className="chat__container">
        {loading ? <Spinner /> : this.renderChat(this.props)}
      </div>
    );
  }
}

const mapStateToProps = ({ chatPage }) => ({
  user: chatPage.user,
  error: chatPage.error,
  loading: chatPage.loading,
  messages: chatPage.messages,
  header: chatPage.header,
  editedMessage: chatPage.editedMessage,
  shouldScrollToBottom: chatPage.shouldScrollToBottom
});

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
