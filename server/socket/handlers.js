export default socket => {
  socket.on("connection", socket => {
    console.log(`New user connected: ${socket.id}`);
  });
  socket.on("chat-action", () => {
    socket.broadcast("chat-update");
  });
};
